my_dict = {'first_algorithm': 'Функция принимает целое число для сравнения.',
           'second_algorithm': 'Функция принимает ввод строковых данных для сравнения.',
           'third_algorithm': 'Функция принимает ввод пользователем целых чисел через пробел.'}


def decorator(function):
    def wrapper():
        for key, value in my_dict.items():
            if function.__name__ == key:
                print(value)

        user_input = input('Пользовательский ввод: ')
        function(user_input)

        print()

    return wrapper


@decorator
def first_algorithm(user_input):
    user_num = int(user_input)

    if user_num > 7:
        print('Привет')


@decorator
def second_algorithm(user_input):
    if user_input == 'Вячеслав':
        print(f'Привет, {user_input}')

    else:
        print('Нет такого имени')


@decorator
def third_algorithm(*args):
    """Функция принимает ввод чисел через пробел (-10 -3 0 1 123 2 (для примера))."""

    for line in args:
        user_list = [int(i) for i in line.split()]
        answer_list = [i for i in user_list if i % 3 == 0]

    print(*answer_list)


def third_algorithm_with_array(example_list):
    print(*[i for i in example_list if i % 3 == 0])


def text_format_task():
    my_answer = """
[((())()(())]]
Исходная скобочная последовательность не верна. Необходимо добавить квадратную 
скобку на крайнюю позицию слева и добавить круглую скобку перед квадратной справа:
[[((())()(()))]] 
Либо удалить крайнюю правую квадратную скобку и крайнюю левую круглую скобку:
[(())()(())]
    """
    with open('text_file.txt', 'w') as file:
        file.write(my_answer)


def main():
    first_algorithm()
    second_algorithm()
    third_algorithm()
    # third_algorithm_with_array([-3, -1, 0, 1, 3, 12, 121])
    text_format_task()


if __name__ == '__main__':
    main()
